#!/bin/bash


sudo yum install epel-release -y
sudo yum install nginx -y
sudo yum install -y php

sudo yum install -y php-mysql php-mbstring php-mcrypt php-devel php-xml php-gd

sudo firewall-cmd --permanent --add-service=http 
sudo firewall-cmd --permanent --add-service=https

sudo firewall-cmd --reload

sudo systemctl start nginx
sudo systemctl enable nginx
sudo systemctl stop nginx

echo "
events {
    worker_connections 1024;
}
http {
    server {
        # Hide nginx version information.
        # server_tokens off;

        listen  80;
        access_log  /var/log/nginx/access.log;
        error_log /var/log/nginx/error.log error;

        location / {
            proxy_pass http://127.0.0.1:8080/;
        }
        

    }
}" > /etc/nginx/nginx.conf


sudo yum install -y httpd
sudo systemctl enable httpd
sudo systemctl restart httpd
sudo sed -i 's/Listen 80/Listen 127.0.0.1:8080/' /etc/httpd/conf/httpd.conf
sudo yum install -y mariadb mariadb-server
sudo  systemctl enable mariadb.service
sudo systemctl start mariadb


sudo systemctl restart httpd
sudo systemctl restart nginx