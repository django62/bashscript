#!/bin/bash
#Добавление правила
sed -i '/OUTPUT ACCEPT/ a -A INPUT -p tcp -m multiport --dports 20,21,60000:65535 -j ACCEPT' /etc/sysconfig/iptables

#Сохранение правил
service iptables save

#Рестарт сервиса 
systemctl restart iptables